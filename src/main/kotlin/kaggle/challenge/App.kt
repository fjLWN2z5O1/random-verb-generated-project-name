package kaggle.challenge


import cursors.*
import cursors.calendar.JvmCal
import cursors.calendar.JvmCal.Iso
import cursors.calendar.JvmCal.Minguo
import cursors.context.NormalizedRange
import cursors.context.Scalar
import cursors.context.TokenizedRow
import cursors.effects.showRandom
import cursors.io.IOMemento.*
import cursors.io.RowVec
import cursors.io.Vect02_.Companion.left
import cursors.io.colIdx
import cursors.io.writeCSV
import cursors.macros.join
import cursors.macros.α
import cursors.ml.DummySpec
import trie.Trie
import vec.macros.*
import vec.util._l
import vec.util._v
import vec.util.debug
import vec.util.println
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.asSequence

/*
annotated CSV timeseries join notebook example.

$ wc -l *csv
     1969 calendar.csv
    30491 sales_train_evaluation.csv
    30491 sales_train_validation.csv
    60981 sample_submission.csv
  6841122 sell_prices.csv
  6965054 total

... 3h20m later...

written 39088168 rows in PT2H14M40.172S 4837/s remaining: PT1H6M17.02861692S est 2020-07-30T23:30:31.746888920

  jim@OJFQCCK22EVHS7YKLYN6LXTEZ2JR4HAZ:/tmp$ ls -sh c6.csv
59G c6.csv

jim@OJFQCCK22EVHS7YKLYN6LXTEZ2JR4HAZ:/tmp$ time wc -l /tmp/c6.csv
58327371 /tmp/c6.csv

real    6m24.867s
user    0m7.213s
sys     1m24.773s
jim@OJFQCCK22EVHS7YKLYN6LXTEZ2JR4HAZ:/tmp$


*/



class App {


    fun notebook() {
        //load walmart dates, for which we will preserve only the walmart specifics and synthesize against more
        // than one cyclical calendar system
        val datesIsam = bootData("/tmp/dates.isam") {
            TokenizedRow.CsvArraysCursor(
                Files.lines(calCsv).asSequence().asIterable(),
                overrides = mapOf(
                    "date" to IoLocalDate,
                    "wday" to IoInt,
                    "month" to IoInt,
                    "year" to IoInt,
                    "snap_CA" to IoInt,
                    "snap_TX" to IoInt,
                    "snap_WI" to IoInt
                )
            )
        }


        //  load the prices table which we will later use a min.max normalization for keras model consumption
        val pricesIsam = bootData("/tmp/prices.isam") {
            // (rows:6841121, [store_id, item_id, wm_yr_wk, sell_price])
            TokenizedRow.CsvArraysCursor(
                Files.lines(sellprice).asSequence().asIterable(),
                overrides = mapOf("sell_price" to IoDouble)
            )
        }

        //  load the walmart sales events.
        val salesIsam = bootData("/tmp/sales.isam") {
            // (rows:30490, [id, item_id, dept_id, cat_id, store_id, state_id, d_1,...]
            TokenizedRow.CsvArraysCursor(Files.lines(salesValCsv).asSequence().asIterable())
        }


        //more destructuring, first one expanded for the reader's benefit. These create an ignored GC link to the filechannel
        // preventing it from closing preserving it until the destructor of the scope, or process.
        val (datesCursor: Cursor, _: Any?) = datesIsam
        val (pricesCursor) = pricesIsam
        val (salesCursor1) = salesIsam
//        val salesCursor = 50_000 t2 salesCursor1.second
        val salesCursor2 = (salesCursor1["state_id", "store_id", "cat_id", "dept_id", "item_id"] α { keyCol ->
            keyCol.toString().split("_").last()
        }).categories(DummySpec.KeepAll).asBitSet()

        val salesCursor3 = salesCursor1[0 until salesCursor1.colIdx["d_1"][0]]

        val salesCursor = join(
            salesCursor3,
            salesCursor2,
            salesCursor1[salesCursor1.colIdx["d_1"][0] until salesCursor1.colIdx.size]
        )

        salesCursor.showRandom()

        println()//these empty prints tend to be where we have debugged something
        println("weekly pricing")

        val nprices = pricesCursor.normalizeDoubleColumn<Double>("sell_price")
        val (_, ctx) = (nprices["normalized:sell_price"] at 0)[0]
        val nr = ctx()[NormalizedRange.normalizedRangeKey]
        nr!! debug "normalized range to ${nr.range.pair}  "
        nprices.showRandom()

        //this is a java LinkedHashMap, being used as a foreign key index later
        val normPriceMapping = nprices.trieOnColumns/*Md4*/("store_id", "item_id", "wm_yr_wk").also { it: Trie ->
            it.freeze()
        }

        println()

        println("---jdates---")

        var cals = _l[Iso, Minguo]
        try {
            ClassLoader.getSystemResource("java.time.chrono.hijrah-config-islamic-umalqura.properties").also {
                cals += JvmCal.Hijrah
            }
        } catch (t: Throwable) {
        }
        val (jdates) = bootData("/tmp/jdatescats.isam") {
            val c = combine(   //concatenation of Vect0r of cursors
                _v[datesCursor[-"weekday", -"wday", -"month", -"year"]],/*vector macro*/
                (cals.map { it: JvmCal -> it.inflate(datesCursor) }).toVect0r<Cursor>() //lazy conversion operator
            )
            val xcal =
                (((cals - Iso).map { it -> -"${it.name}:DAY_OF_WEEK" }) + _l[-"wm_yr_wk", -"d", -"date"]).toTypedArray()
            val jdates: Cursor =
                join( /* positional join of n cursors*/ c)// all together, we have an inner loop of Vect0rs of Cursors forming one Vect0r of Cursors which are joined positionally
            jdates.showRandom() //head 1 row
            join(jdates["date", "wm_yr_wk", "d", "snap_CA", "snap_TX", "snap_WI"], jdates.get(* xcal).categories())
        }
        println("7 days per wm_wk except where sales were 0 not yet reified -- dates")

        println()
        //        salesCursor.showRandom()
        //        val events=salesCursor.group()
        val ints = salesCursor.colIdx["d_1"]
        val spliceAfter = ints[0]
        val datecount = salesCursor.colIdx.size - spliceAfter
        val sc2 = salesCursor[0 until spliceAfter]
        sc2.showRandom()
        val tmpScalar: () -> Scalar = Scalar(IoFloat, "amt").`⟲`

        //roughly speaking this says "repeat these rows datecount number of times.", but does not move any data.
        val sales1: Cursor = combine(Vect0r(datecount) { _: Int -> sc2 })


        // this is longhand Cursor construction to use the y-th row after "d_1" as a value column
        // for the time-series event row being shown
        val depivotDates: Cursor = combine(Vect0r(datecount) { y: Int ->
            Cursor(sc2.size) { iy: Int ->
                val sc3: RowVec = (salesCursor[spliceAfter + y] at iy)
                RowVec(1) { _: Int -> sc3.left[0] t2 tmpScalar }
            }
        })

        // a destructuring declaration to pluck the function  right out of the (jdates)  Cursor (a Vect0r or RowVec's)
        // and distort the requested Row Index
        val (_: Int, f: (Int) -> RowVec) = jdates
        val sourceDates1a: Cursor = sc2.size t2 { f(it / jdates.size) }

        // this is a shorthand for what creates c1, leveraging where Vect0r is a Pai2
        // Pai2, which is created by "A t2 B" like kotlin's Pair created by "A to B"

        val sourceDates: Cursor = combine((datecount) t2 { _: Int -> sourceDates1a })

        this println _l[sales1, depivotDates, sourceDates].map(Cursor::size)

        //join the rows of the three previous examples  and show the header
        val c4 = join(sales1, depivotDates, sourceDates)
        c4.showRandom()

        //join in the prices by map lookup, basically a multicolumn fkey to sell_price which neeeded a small detour
        // first for min/max
        val collator = { ->
            val nanSentinel =
                TokenizedRow.CsvArraysCursor(_l["normalized:sell_price", "-1"], _v[IoFloat]) at 0
            val justPriceColumn = nprices["normalized:sell_price"]
            val keyCurs = c4["store_id", "item_id", "wm_yr_wk"]

            val justTheMinMaxPrices: Cursor = c4.size t2 { iy: Int ->
                val key = (keyCurs at iy).left.α(Any?::toString).toArray()
                val i = normPriceMapping.get(*key)
                i?.let {
                    justPriceColumn at i
                } ?: nanSentinel
            }
            justTheMinMaxPrices
        }
        val pathname = "/tmp/mmprices.isam"
        val (justTheMinMaxPrices) = bootData(pathname, collator)
        justTheMinMaxPrices.showRandom()
        //we're done joining keys, so we can elimnate them
        val c6 = join(
            c4[c4.colIdx["date"] + c4.colIdx[-"date", -"wm_yr_wk", -"d", -"id", -"item_id", -"dept_id", -"cat_id", -"store_id", -"state_id"]],
            justTheMinMaxPrices
        )
        this println "c6 ${c6.size} rows"
        c6.showRandom()
        c6.writeCSV("/tmp/c6.csv")
    }

    companion object {
        /**
         * the file locations, set up here for the maven project structure being used, masquerading as a unit test right now.
         *
         */

        val dir = Paths.get("src/test/resources")
        val calCsv = dir.resolve("calendar.csv")
        val sellprice = dir.resolve("sell_prices.csv")
        val salesValCsv = dir.resolve("sales_train_validation.csv")
        val salesEvalCsv = dir.resolve("sales_train_evaluation.csv")
        // the first three  kotlin run scopes are boilerplate load/save conversions
        // something like 6 gigs is used in memroy for the  third scope, pending a future iteration with mmap backing
        // store on csv files.

        //        fun  bootData(  path:String,f:()->Cursor):Pai2<Cursor,*> = localLillyPad(path,f) as Pai2<Cursor, *>
        fun bootData(path: String, f: () -> Cursor): Pai2<Cursor, *> = f().let { it: Cursor ->
            it t2 Unit
        }

        @JvmStatic
        fun main(args: Array<String>) {
            App().notebook()
        }
    }
}
